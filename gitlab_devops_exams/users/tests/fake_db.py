import unittest

class Testing2(unittest.TestCase):
    def test_string_2(self):
        a = 'some'
        b = 'some'
        self.assertEqual(a, b)

    def test_boolean_2(self):
        a = True
        b = True
        self.assertEqual(a, b)

if __name__ == '__main__':
    unittest.main()