 # Déploiement en production

 Le déploiement en production est effectué si, et seulement si, ceci est demandé par une action manuelle.

 Par ailleurs, celui-ci est disponible si, et seulement si, la branche concernée est la branche 'main'.